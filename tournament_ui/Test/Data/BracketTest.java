package Data;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BracketTest {
    Bracket testBracket;
    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testAddRound()
    {
        List<Team>  teams = setTestTeams(4);
        testBracket = new Bracket(teams);
        int numRounds = testBracket.getRounds().size();
        testBracket.newRound();
        Assert.assertEquals(numRounds+1, testBracket.getRounds().size());
    }

    @Test
    public void testSetBracket4()
    {
        int numTeams = 4;
        List<Team> teams = setTestTeams(numTeams);
        testBracket = new Bracket(teams);
        testBracket.setBracket();
        Assert.assertEquals(numTeams,testBracket.getTeams().size());
        Assert.assertEquals(0, testBracket.getByeTeams().size());
        Assert.assertEquals(4,testBracket.getNextRoundTeams().size());
    }
    @Test
    public void testSetBracket6()
    {
        int numTeams = 6;
        List<Team> teams = setTestTeams(numTeams);
        testBracket = new Bracket(teams);
        testBracket.setBracket();
        Assert.assertEquals(numTeams,testBracket.getTeams().size());
        Assert.assertEquals(2, testBracket.getByeTeams().size());
        Assert.assertEquals(4,testBracket.getNextRoundTeams().size());
    }
    @Test
    public void testSetBracket8()
    {
        int numTeams = 8;
        List<Team> teams = setTestTeams(numTeams);
        testBracket = new Bracket(teams);
        testBracket.setBracket();
        Assert.assertEquals(numTeams,testBracket.getTeams().size());
        Assert.assertEquals(0, testBracket.getByeTeams().size());
        Assert.assertEquals(8,testBracket.getNextRoundTeams().size());
    }
    @Test
    public void testSetBracket11()
    {
        int numTeams = 11;
        List<Team> teams = setTestTeams(numTeams);
        testBracket = new Bracket(teams);
        testBracket.setBracket();
        Assert.assertEquals(numTeams,testBracket.getTeams().size());
        Assert.assertEquals(5, testBracket.getByeTeams().size());
        Assert.assertEquals(6,testBracket.getNextRoundTeams().size());
    }

    @Test
    public void testPlayCurrentGame()
    {
        int numTeams = 8;
        List<Team> teams = setTestTeams(numTeams);
        testBracket = new Bracket(teams);
        testBracket.setBracket();
        List<Game> testGameList = testBracket.getRounds().get(0).getGames();
        for(Game game : testGameList)
        {
            testBracket.playCurrentGame(game.getTeam1(), game.getTeam2());
        }
        List<Team> testWinners = testBracket.getNextRoundTeams();
        Assert.assertEquals(4,testWinners.size());
        for(int i = 0; i < testWinners.size();i++)
        {
            Assert.assertEquals("team"+(i+1),testWinners.get(i).getName());
        }

    }

    public List<Team> setTestTeams(int numTeams)
    {
        List<Team>  teams = new ArrayList<>();
        for (int i = 1; i < numTeams+1; i++)
        {
            teams.add(new Team("team"+i, i));
        }
        return teams;
    }
    @After
    public void tearDown() throws Exception {
    }
}