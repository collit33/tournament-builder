package Data;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SeasonTest {

    Season season = new Season();
    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void setTeams0() {
        int numTeams = 0;
        List<String> teamNames = setTeamNames(numTeams);
        String message = season.setTeams(teamNames);
        Assert.assertEquals("There are not enough teams to run this tournament", message);
    }
    @Test
    public void setTeams3() {
        int numTeams = 3;
        List<String> teamNames = setTeamNames(numTeams);
        String message = season.setTeams(teamNames);
        Assert.assertEquals("There are not enough teams to run this tournament", message);
    }
    @Test
    public void setTeams4() {
        int numTeams = 4;
        List<String> teamNames = setTeamNames(numTeams);
        String message = season.setTeams(teamNames);
        Assert.assertEquals("The teams are set", message);
    }
    @Test
    public void setTeams5() {
        int numTeams = 5;
        List<String> teamNames = setTeamNames(numTeams);
        String message = season.setTeams(teamNames);
        Assert.assertEquals("The teams are set", message);
    }
    @Test
    public void setTeams34() {
        int numTeams = 34;
        List<String> teamNames = setTeamNames(numTeams);
        String message = season.setTeams(teamNames);
        Assert.assertEquals("The teams are set", message);
    }
    @Test
    public void setTeams63() {
        int numTeams = 63;
        List<String> teamNames = setTeamNames(numTeams);
        String message = season.setTeams(teamNames);
        Assert.assertEquals("The teams are set", message);
    }
    @Test
    public void setTeams64() {
        int numTeams = 64;
        List<String> teamNames = setTeamNames(numTeams);
        String message = season.setTeams(teamNames);
        Assert.assertEquals("The teams are set", message);
    }
    @Test
    public void setTeams65() {
        int numTeams = 65;
        List<String> teamNames = setTeamNames(numTeams);
        String message = season.setTeams(teamNames);
        Assert.assertEquals("There are too many teams to run this tournament", message);
    }
    @Test
    public void setTeams100() {
        int numTeams = 100;
        List<String> teamNames = setTeamNames(numTeams);
        String message = season.setTeams(teamNames);
        Assert.assertEquals("There are too many teams to run this tournament", message);
    }

    @Test
    public void setSeasonGames4() {
        int numTeams = 4;
        List<String> teamNames = setTeamNames(numTeams);
        season.setTeams(teamNames);
        season.setSeasonGames();
        Assert.assertEquals(6, season.getGames().size());
    }

    @Test
    public void setSeasonGames5() {
        int numTeams = 5;
        List<String> teamNames = setTeamNames(numTeams);
        season.setTeams(teamNames);
        season.setSeasonGames();
        Assert.assertEquals(10, season.getGames().size());
    }

    @Test
    public void switchTeamsAdjacentTest()
    {
        int numTeams = 4;
        List<String> teamNames = setTeamNames(numTeams);
        season.setTeams(teamNames);
        List<Team> teams = season.getTeams();
        int id1 = teams.get(0).getId();
        int id2 = teams.get(1).getId();
        season.switchTeams(teams.get(0), teams.get(1));
        Assert.assertTrue(teams.get(0).getId()==id2);
        Assert.assertTrue(teams.get(1).getId()==id1);
    }

    @Test
    public void switchTeamsOppositeTest()
    {
        int numTeams = 4;
        List<String> teamNames = setTeamNames(numTeams);
        season.setTeams(teamNames);
        List<Team> teams = season.getTeams();
        int id1 = teams.get(0).getId();
        int id2 = teams.get(3).getId();
        season.switchTeams(teams.get(0), teams.get(3));
        Assert.assertTrue(teams.get(0).getId()==id2);
        Assert.assertTrue(teams.get(3).getId()==id1);
    }

    @Test
    public void findTeamTestBegin()
    {
        int numTeams = 10;
        int expected = 0;
        List<String> teamNames = setTeamNames(numTeams);
        season.setTeams(teamNames);
        Team team = season.getTeams().get(expected);
        int index = season.findTeamInList(team);
        Assert.assertEquals(expected, index);
    }

    @Test
    public void findTeamTestMiddle()
    {
        int numTeams = 10;
        int expected = 5;
        List<String> teamNames = setTeamNames(numTeams);
        season.setTeams(teamNames);
        Team team = season.getTeams().get(expected);
        int index = season.findTeamInList(team);
        Assert.assertEquals(expected, index);
    }

    @Test
    public void findTeamTestEnd()
    {
        int numTeams = 10;
        int expected = 9;
        List<String> teamNames = setTeamNames(numTeams);
        season.setTeams(teamNames);
        Team team = season.getTeams().get(expected);
        int index = season.findTeamInList(team);
        Assert.assertEquals(expected, index);
    }

    @Test
    public void findTeamTestDoesNotExist()
    {
        int numTeams = 10;
        int expected = -1;
        List<String> teamNames = setTeamNames(numTeams);
        season.setTeams(teamNames);
        int index = season.findTeamInList(new Team("Imposter",25));
        Assert.assertEquals(expected, index);
    }

    @Test
    public void playGameTest()
    {
        int numTeams = 5;
        List<String> teamNames = setTeamNames(numTeams);
        season.setTeams(teamNames);
        season.setSeasonGames();
        Game game = season.getCurrentGame();
        Team winner = game.getTeam1();
        Team loser = game.getTeam2();
        season.playGame(winner, loser);
        Assert.assertEquals(1, winner.getRank());
        Assert.assertEquals(5, loser.getRank());
        Assert.assertEquals(false, season.isTourneyStarted());
    }

    @Test
    public void wholeSeasonTest()
    {
        int numTeams = 5;
        List<String> teamNames = setTeamNames(numTeams);
        season.setTeams(teamNames);
        season.setSeasonGames();
        List<Game> games = season.getGames();
        for (Game game : games)
        {
            Team winner = game.getTeam1();
            Team loser = game.getTeam2();
            season.playGame(winner, loser);
        }
        Assert.assertEquals(true, season.isTourneyStarted());
    }

    public List<String> setTeamNames(int numTeams)
    {
        List<String> names = new ArrayList<>();
        for (int i = 0; i < numTeams; i++)
        {
            names.add("team"+i);
        }
        return names;
    }
}