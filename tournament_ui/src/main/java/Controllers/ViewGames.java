package Controllers;

import Data.Game;
import Data.Round;
import Data.Season;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;

public class ViewGames {
    private Season season;
    private boolean tourneyStarted;
    @FXML
    public Label title = new Label();
    @FXML
    public TextArea gameList = new TextArea();
    @FXML
    public Button back = new Button();
    @FXML
    public void backPressed(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controllers/PlayGames.fxml"));
        Parent root = loader.load();

        PlayGames playGames = loader.getController();
        playGames.start(season, this.tourneyStarted);

        Scene newScene = new Scene(root);
        Stage stage = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        stage.setScene(newScene);
        stage.show();
    }

    public void start(Season season, boolean tourneyStarted) {
        this.season = season;
        this.tourneyStarted = tourneyStarted;
        if (this.tourneyStarted)
        {
            Round current = this.season.getBracket().getCurrentRound();
            appendToGameList(current.getGames());
            this.title.setText("Round "+this.season.getBracket().getRounds().size()+" Games");
        }
        else
        {
            appendToGameList(this.season.getGames());
        }
    }

    private void appendToGameList(List<Game> games)
    {
        for(Game game : games)
        {
            gameList.appendText(game.toString()+"\n");
        }
    }
}
