package Controllers;
import Data.Season;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;

public class Startup extends Application {

    Season season = new Season();
    @FXML
    public Label title = new Label();
    @FXML
    public Button start_btn = new Button();

    @Override
    public void start(Stage primaryStage) throws IOException {
        //Parent root = FXMLLoader.load(getClass().getResource("/GUI/Startup.fxml"));
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Controllers/Startup.fxml"));
        Parent root = fxmlLoader.load();
        primaryStage.setTitle("Tournament Builder");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();
    }

    @FXML
    public void startPressed(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controllers/SetTeams.fxml"));
        Parent root = loader.load();

        SetTeams setTeams = loader.getController();
        setTeams.setSeason(this.season);

        Scene newScene = new Scene(root);
        Stage stage = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        stage.setScene(newScene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
