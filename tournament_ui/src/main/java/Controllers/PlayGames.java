package Controllers;

import Data.Season;
import Data.Team;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;

public class PlayGames {
    private Season season;
    private boolean tourneyStarted;

    @FXML
    public Label game = new Label();
    @FXML
    public TextArea standings = new TextArea();
    @FXML
    public Button playGame = new Button();
    @FXML
    public Button viewGames = new Button();

    @FXML
    public void playGamePressed(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controllers/CurrentGame.fxml"));
        Parent root = loader.load();

        CurrentGame currentGame = loader.getController();
        currentGame.start(season, tourneyStarted);

        Scene newScene = new Scene(root);
        Stage stage = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        stage.setScene(newScene);
        stage.show();
    }
    @FXML
    public void viewGamesPressed(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controllers/ViewGames.fxml"));
        Parent root = loader.load();

        ViewGames viewGames = loader.getController();
        viewGames.start(season, tourneyStarted);

        Scene newScene = new Scene(root);
        Stage stage = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        stage.setScene(newScene);
        stage.show();
    }

    public void start(Season season, boolean tourneyStarted)
    {
        this.season = season;
        this.tourneyStarted = tourneyStarted;
        for(Team team : this.season.getTeams())
        {
            standings.appendText(team.toString()+"\n");
        }
        game.setText(this.season.getCurrentGame().toString());
    }
}
