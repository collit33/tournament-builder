package Controllers;

import Data.Season;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;

public class SetTeams {
    private Season season;
    private List<String> teams;

    @FXML
    public TextField newTeam = new TextField();
    @FXML
    public ChoiceBox teamList = new ChoiceBox();
    @FXML
    public Button next = new Button();
    @FXML
    public Button remove = new Button();
    @FXML
    public Button add = new Button();
    @FXML
    public Label error = new Label();

    /**
     * TODO
     * Add a non-editable text area that displays all the current teams added
     * @param event
     */
    @FXML
    public void addTeam(ActionEvent event)
    {
        if(newTeam.getText().equals(""))
        {
            error.setText("Please enter a name");
            error.setVisible(true);
        }
        else if(findNameInList(newTeam.getText()))
        {
            error.setText("That name is already entered");
            error.setVisible(true);
        }
        else
        {
            turnOffError();
            teamList.getItems().add(newTeam.getText());
            newTeam.setText("");
        }
    }

    public void removeTeam(ActionEvent event)
    {
        teamList.getItems().remove(teamList.getSelectionModel().getSelectedItem());
    }

    public void nextPressed(ActionEvent event) throws IOException {
        teams = teamList.getItems();
        String message = season.setTeams(teams);
        if (message == "The teams are set")
        {
            turnOffError();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controllers/SetGames.fxml"));
            Parent root = loader.load();

            SetGames setGames = loader.getController();
            setGames.setSeason(this.season);

            Scene newScene = new Scene(root);
            Stage stage = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
            stage.setScene(newScene);
            stage.show();
        }
        else
        {
            error.setText(message);
            error.setVisible(true);
        }
    }

    public boolean findNameInList(String name)
    {
        int count = 0;
        boolean found = false;
        while(!found && count < teamList.getItems().size())
        {
            if(newTeam.getText().equals(teamList.getItems().get(count)))
            {
                found = true;
            }
            count++;
        }
        return found;
    }
    public void turnOffError()
    {
        if (error.isVisible())
        {
            error.setVisible(false);
        }
    }
    public void setSeason(Season season) {
        this.season = season;
    }
}
