package Controllers;

import Data.Season;
import Data.Team;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;

public class SetGames {
    private Season season;
    private int bracketType;

    @FXML
    public Button next = new Button();
    @FXML
    public Button seasonBtn = new Button();
    @FXML
    public Button randomBtn = new Button();
    @FXML
    public Button manualBtn = new Button();
    @FXML
    public ListView teamList = new ListView();
    @FXML
    public Label instruction = new Label();

    @FXML
    public void seasonPressed()
    {
        this.bracketType = 1;
        randomBtn.setDisable(true);
        seasonBtn.setDisable(true);
        manualBtn.setDisable(true);
        next.setDisable(false);
        season.setSeasonGames();
        instruction.setText("Click Next");
    }

    @FXML
    public void randomPressed()
    {
        this.bracketType = 2;
        randomBtn.setVisible(false);
        seasonBtn.setVisible(false);
        manualBtn.setVisible(false);
        next.setDisable(false);
        //Call setBracket(shuffles teams and sets rank)
        season.setBracket(bracketType);
        //Call displayTeams (puts the toString value of each team into the ListView)
        displayTeams();
        //Set TeamList to visible
        teamList.setVisible(true);
        instruction.setText("Click Next");
    }

    @FXML
    public void manualPressed()
    {
        this.bracketType = 3;
        randomBtn.setVisible(false);
        seasonBtn.setVisible(false);
        manualBtn.setVisible(false);
        next.setDisable(false);
        //Call display to get all the teams
        displayTeams();
        //Set teamList visible and editable
        teamList.setVisible(true);
        teamList.setEditable(true);
        instruction.setText("Arrange teams to desired ranking, then click next");
    }

    /**
     * TODO
     * Once SetGames is working, create the next layer after PlayGames and finish this function for random and manual
     * Find a List view that can be rearranged manually
     * Shuffle teams multiple times for random
     * @param event
     * @throws IOException
     */
    @FXML
    public void nextPressed(ActionEvent event) throws IOException {
        if(bracketType == 1)
        {
            //Go to Play Games
        }
        else
        {
            //Start tournament
            //If bracket type is 3: call setBracket
        }


        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controllers/PlayGames.fxml"));
        Parent root = loader.load();

        PlayGames playGames = loader.getController();
        playGames.start(season, false);

        Scene newScene = new Scene(root);
        Stage stage = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        stage.setScene(newScene);
        stage.show();
    }

    public void displayTeams()
    {
        for(Team team : season.getTeams()) {
            teamList.getItems().add(team.toString());
        }
    }
    public void setSeason(Season season) {
        this.season = season;
    }
}
