package Controllers;

import Data.Game;
import Data.Season;
import Data.Team;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;

public class CurrentGame {
    private Season season;
    private Game game;
    private Team winner;
    private Team loser;
    private boolean tourneyStarted;

    @FXML
    public Button t1Btn = new Button();
    @FXML
    public Button t2Btn = new Button();
    @FXML
    public Button next = new Button();

    @FXML
    public void t1Pressed()
    {
        winner = game.getTeam1();
        loser = game.getTeam2();
        t1Btn.setDisable(true);
        t2Btn.setDisable(true);
        next.setDisable(false);
    }
    @FXML
    public void t2Pressed()
    {
        winner = game.getTeam2();
        loser = game.getTeam1();
        t1Btn.setDisable(true);
        t2Btn.setDisable(true);
        next.setDisable(false);
    }
    @FXML
    public void nextPressed(ActionEvent event) throws IOException {
        season.playGame(winner, loser);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controllers/PlayGames.fxml"));
        Parent root = loader.load();

        PlayGames playGames = loader.getController();
        playGames.start(season, this.tourneyStarted);

        Scene newScene = new Scene(root);
        Stage stage = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        stage.setScene(newScene);
        stage.show();
    }

    public void start(Season season, boolean tourneyStarted) {
        this.season = season;
        this.tourneyStarted = tourneyStarted;
        this.game = this.season.getCurrentGame();
        t1Btn.setText(this.game.getTeam1().getName());
        t2Btn.setText(this.game.getTeam2().getName());
    }
}
