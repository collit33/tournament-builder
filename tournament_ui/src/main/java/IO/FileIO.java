package IO;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileIO
{
    private String path;

    public FileIO(String path)
    {
        this.path = path;
    }

    public List<String> readFile()
    {
        List<String> text = new ArrayList<>();
        try
        {
            File file = new File(this.path);
            Scanner reader = new Scanner(file);
            while(reader.hasNextLine())
            {
                text.add(reader.nextLine());
            }
            reader.close();

        } catch (FileNotFoundException e){
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return text;
    }
    public void createFile()
    {
        try
        {
            File file = new File(this.path);
            file.createNewFile();
        }catch (IOException e){
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
    public void writeFile(List<String> text)
    {
        try
        {
            FileWriter writer = new FileWriter(this.path);
            for(String line : text)
            {
                writer.write(line + "\n");
            }
            writer.close();
        }catch (IOException e){
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public void deleteFile()
    {
        File file = new File(path);
        file.delete();
    }

    public File[] getFileList()
    {
        File f = new File(this.path);
        return f.listFiles();
    }

    public String getPath() {
        return path;
    }
}
