package Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Bracket
{
    private List<Round> rounds;
    private List<Team> teams;
    private List<Team> nextRoundTeams;
    private List<Team> byeTeams;

    public Bracket (List<Team> teams)
    {
        this.rounds = new ArrayList<>();
        this.teams = teams;
        this.nextRoundTeams = new ArrayList<>();
        this.byeTeams = new ArrayList<>();
    }

    /**
     * This function adds a new round to the bracket.
     * To be called after the last round is finished (provided there are greater than one teams left)
     * Also called at the beginning of the tournament
     */
    public void newRound()
    {
        rounds.add(new Round(nextRoundTeams));
    }

    /**
     * This function sets the first round of a bracket.
     * If the tournament has an even number of teams(that is, a power of 2 teams), it will simply add a new round
     * If the tournament is odd, it will identify the necessary number of teams that will have a bye.
     */
    public void setBracket()
    {
        int numteams = teams.size();
        int power2 = 2;
        while (power2 < numteams)
        {
            power2 = power2*2;
        }
        if (power2 == numteams)
        {
            nextRoundTeams = teams;
            newRound();
        }
        else
        {
            int round1games = (numteams-(power2/2));
            int numbyes = numteams-(round1games*2);
            for (int i = 0; i < teams.size();i++)
            {
                if (i<numbyes)
                {
                    byeTeams.add(teams.get(i));
                }
                else
                {
                    nextRoundTeams.add(teams.get(i));
                }
            }
            newRound();
        }
    }

    /**
     * This function calls the play game function in Round
     * It also ends the current round and creates a new one if the last game of the round is played.
     * @param winner
     * @param loser
     */
    public void playCurrentGame(Team winner, Team loser)
    {
        Round currentRound = getCurrentRound();
        currentRound.playGame(winner, loser);
        if (currentRound.isEndRound())
        {
            nextRoundTeams = currentRound.getWinners();
            newRound();
        }
    }

    public Round getCurrentRound()
    {
        return rounds.get(rounds.size()-1);
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public List<Team> getNextRoundTeams() {
        return nextRoundTeams;
    }

    public List<Team> getByeTeams() {
        return byeTeams;
    }
}
