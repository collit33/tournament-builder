package Data;

import java.util.ArrayList;
import java.util.List;

public class Team
{
    private final double INITIAL_PCT = 0.5;
    private int rank;
    private String name;
    private int id;
    private int wins;
    private int losses;
    private double winPercent;
    private List<Team> teamsBeaten;

    public Team(String name, int id)
    {
        this.id = id;
        this.name = name;
        this.wins = 0;
        this.losses = 0;
        this.winPercent = INITIAL_PCT;
        teamsBeaten = new ArrayList<>();
    }

    public Team(String name,int id, int rank)
    {
        this.name = name;
        this.rank = rank;
        this.wins = 0;
        this.losses = 0;
        this.winPercent = INITIAL_PCT;
        teamsBeaten = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    public void win(Team loser)
    {
        wins++;
        winPercent();
        teamsBeaten.add(loser);
    }

    public void lose()
    {
        losses++;
        winPercent();
    }

    public void winPercent()
    {
        winPercent = (double)(wins) / (wins + losses);
    }

    public int getRank() {
        return rank;
    }

    public int getId() {
        return id;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public double getWinPercent() {
        return winPercent;
    }


    public boolean checkIfBeaten(Team beaten) {
        boolean found = false;
        int count = 0;
        while (!found && count<teamsBeaten.size())
        {
            if(teamsBeaten.get(count).getId() == beaten.getId())
            {
                found = true;
            }
            count++;
        }
        return found;
    }

    public boolean checkIfBetterThan(Team team)
    {
        boolean betterThan = false;
        if(this.winPercent > team.getWinPercent())
        {
            betterThan = true;
        }
        else if(this.winPercent == team.getWinPercent())
        {
            boolean iWon = this.checkIfBeaten(team);
            boolean theyWon = team.checkIfBeaten(this);
            if(iWon)
            {
                betterThan = true;
            }
            else if(!theyWon)
            {
                if (this.findHighestTeamBeaten() < team.findHighestTeamBeaten())
                {
                    betterThan = true;
                }
            }
        }
        return betterThan;
    }

    public int findHighestTeamBeaten()
    {
        int highestRank = 65; //Needs to be a number bigger than the lowest possible rank
        for (Team team: teamsBeaten)
        {
            if (team.getRank() < highestRank)
            {
                highestRank = team.getRank();
            }
        }
        return highestRank;
    }

    @Override
    public String toString() {
        return rank + ". " + name + ": " + wins + "-" + losses;
    }
}
