package Data;

public class Game
{
    private Team team1;
    private Team team2;
    private Team winner;
    private Team loser;
    private boolean played;

    public Game(Team team1, Team team2)
    {
        this.played = false;
        this.team1 = team1;
        this.team2 = team2;
    }

    @Override
    public String toString() {
        String game = team1.getName() + " vs. " + team2.getName();
        if (played)
        {
            game = game + ": " + winner.getName();
        }
        return game;
    }

    public void playGame(Team winner, Team loser)
    {
        played = true;
        winner.win(loser);
        loser.lose();
        this.winner = winner;
        this.loser = loser;
    }

    public Team getWinner() {
        return winner;
    }

    public Team getTeam1() {
        return team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public Team getLoser() {
        return loser;
    }
}