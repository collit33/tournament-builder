package Data;

public class Startup
{
    /**
     * TODO
     * Input: a list of teams
     * Have program generate a list of games and output as a file
     * Use completed file to play all games
     * Output: a list of teams ranked
     * @param args
     */
    public static void main(String[] args) {
        Season season = new Season();
        season.run();
    }
}
