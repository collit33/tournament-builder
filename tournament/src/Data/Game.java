package Data;

public class Game
{
    private Team team1;
    private Team team2;
    private Team winner;
    private Team loser;

    public Game(Team team1, Team team2)
    {
        this.team1 = team1;
        this.team2 = team2;
    }

    @Override
    public String toString() {
        return team1.getName() + " vs. " + team2.getName();
    }

    public void playGame(Team winner, Team loser)
    {
        winner.win(loser);
        loser.lose();
        this.winner = winner;
        this.loser = loser;
    }

    public Team getWinner() {
        return winner;
    }

    public Team getTeam1() {
        return team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public Team getLoser() {
        return loser;
    }
}