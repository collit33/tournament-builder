package Data;

import IO.FileIO;

import java.util.*;

public class Season
{
    private Bracket bracket;
    private List<Team> teams;
    private List<Game> games;
    private Scanner in = new Scanner(System.in);
    private int currentGameIndex;
    private FileIO teamList;
    private FileIO gameList;
    private FileIO rankings;


    /**
     * Input team names and add them to the list
     */
    public Season()
    {
        teams = new ArrayList<>();
        games = new ArrayList<>();
        currentGameIndex = 0;
        teamList = new FileIO("teams.txt");
        gameList = new FileIO("gameList.txt");
        rankings = new FileIO("rankings.txt");
    }

    /**
     * This function creates team objects with a number of team names.
     * The function checks to make sure the number of teams is between 4 and 64
     * @param teamNames
     */
    public String setTeams(List<String> teamNames)
    {
        String message = "";
        if (teamNames.size() < 4)
        {
            message = "There are not enough teams to run this tournament";
        }
        else if (teamNames.size() > 64)
        {
            message = "There are too many teams to run this tournament";
        }
        else
        {
            for (int i = 0; i < teamNames.size(); i++)
            {
                teams.add(new Team(teamNames.get(i),i+1));
            }
            message = "The teams are set";
        }
        return message;
    }

    /**
     * This function matches up each team against every other team, then shuffles the matchups
     */
    public void setSeasonGames()
    {
        for (int i = 0; i<teams.size()-1; i++)
        {
            for (int j = i+1; j<teams.size();j++)
            {
                games.add(new Game(teams.get(i),teams.get(j)));
            }
        }
        Collections.shuffle(games);
    }

    public void addGamesFromFile(List<String> gameStrings)
    {
        List<Team> winners = new ArrayList<>();
        for (String game : gameStrings)
        {
            String[] splitByWinner = game.split(": ");
            String[] teams = splitByWinner[0].split(" vs. ");
            games.add(new Game(findTeamInList(teams[0]),findTeamInList(teams[1])));
            winners.add(findTeamInList(splitByWinner[1]));
        }
        for (Game game : games)
        {
            Team team1 = game.getTeam1();
            Team team2 = game.getTeam2();
            if(winners.get(currentGameIndex).getId() == team1.getId())
            {
                this.playGame(team1, team2);
            }
            else
            {
                this.playGame(team2, team1);
            }
        }
    }
    /**
     * This function sets a winner and loser to a game in the list of games at the current game index
     * It then calls functions to rank the teams, and set the rank numbers\
     * Finally, it increments the current game index and ends the season if there are no more games.
     */
    public boolean playGame(Team winner, Team loser)
    {
        boolean endSeason = false;
        games.get(currentGameIndex).playGame(winner, loser);
        rankTeams(winner, true);
        rankTeams(loser, false);
        setRanks();
        currentGameIndex++;
        if (currentGameIndex == games.size())
        {
            endSeason = true;
        }
        return endSeason;
    }

    /**
     * This function ranks the teams
     * @param team
     * @param winner
     */
    public void rankTeams(Team team, boolean winner)
    {
        int index = findTeamInList(team);
        boolean done = false;
        if(winner)
        {
            index--;
            while(!done && index >= 0)
            {
                if(team.checkIfBetterThan(teams.get(index)))
                {
                    switchTeams(team, teams.get(index));
                    index--;
                }
                else
                {
                    done = true;
                }
            }
        }
        else
        {
            index++;
            while(!done && index < teams.size())
            {
                if(teams.get(index).checkIfBetterThan(team))
                {
                    switchTeams(team, teams.get(index));
                    index++;
                }
                else
                {
                    done = true;
                }
            }
        }
    }

    /**
     * This function actually sets the numerical rank value to each team.
     */
    public void setRanks()
    {
        int rank = 1;
        for (Team team : teams)
        {
            team.setRank(rank);
            rank++;
        }
    }

    /**
     * This function switches the position of two teams in the teams list
     * @param team1
     * @param team2
     */
    public void switchTeams(Team team1, Team team2)
    {
        int index1 = findTeamInList(team1);
        int index2 = findTeamInList(team2);
        Collections.swap(teams,index1,index2);
    }

    /**
     * Finds a team in the list of teams and returns the index
     * @param team
     * @return
     */
    public int findTeamInList(Team team)
    {
        boolean found = false;
        int count = 0;
        while(!found && count < this.teams.size())
        {
            if(team.getId() == this.teams.get(count).getId())
            {
                found = true;
                count--;
            }
            count++;
        }
        if (!found)
        {
            count = -1;
        }
        return count;
    }

    public Team findTeamInList(String name)
    {
        boolean found = false;
        int count = 0;
        while(!found && count < this.teams.size())
        {
            if(name.equals(this.teams.get(count).getName()))
            {
                found = true;
                count--;
            }
            count++;
        }
        if (!found)
        {
            count = -1;
        }
        return teams.get(count);
    }

    /**
     * Function called from program's startup
     */
    public void run()
    {
        /* // This sets the list of games. Then manually type in the winners into the text file
        List<String> names = teamList.readFile();
        setTeams(names);
        setSeasonGames();
        List<String> gameStrings = new ArrayList<>();
        for (Game game : games)
        {
            gameStrings.add(game.toString());
        }
        gameList.createFile();
        gameList.writeFile(gameStrings);

         */

         //This reads in the completed game list file and ranks the teams.
        List<String> names = teamList.readFile();
        setTeams(names);
        List<String> gameStrings = gameList.readFile();
        addGamesFromFile(gameStrings);
        List<String> teamStrings = new ArrayList<>();
        for (Team team: teams)
        {
            teamStrings.add(team.toString());
        }
        rankings.createFile();
        rankings.writeFile(teamStrings);
    }

    public List<Game> getGames() {
        return games;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public int getCurrentGameIndex() {
        return currentGameIndex;
    }
}
