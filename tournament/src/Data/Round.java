package Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Round
{
    private List<Game> games = new ArrayList<>();
    private List<Team> teams;
    private List<Team> winners;
    private int currentGameIndex = 0;
    private boolean endRound;

    public Round(List<Team> teams)
    {
        this.winners = new ArrayList<>();
        this.endRound = false;
        this.teams = teams;
        int i = 0;
        int j = this.teams.size() - 1;
        while(i<j)
        {
            games.add(new Game(teams.get(i), teams.get(j)));
            i++;
            j--;
        }
    }

    public void playGame(Team winner, Team loser)
    {
        Game currentGame = games.get(this.currentGameIndex);
        currentGame.playGame(winner, loser);
        winners.add(winner);
        this.currentGameIndex++;
        if (this.currentGameIndex == games.size())
        {
            endRound = true;
        }
    }

    public boolean isEndRound() {
        return endRound;
    }

    public List<Team> getWinners() {
        return winners;
    }

    public List<Game> getGames() {
        return games;
    }
}
