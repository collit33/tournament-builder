package IO;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class FileIOTest {

    FileIO fileIO;
    @Before
    public void setUp() throws Exception {
        fileIO = new FileIO("test.txt");
    }

    @After
    public void tearDown() throws Exception {
        fileIO.deleteFile();
    }

    @Test
    public void readWriteTest()
    {
        List<String> text = new ArrayList<>();
        text.add("This is a test");
        String expected = "This is a test";
        fileIO.writeFile(text);
        List<String> readText = fileIO.readFile();
        String actual = readText.get(0);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void readWriteTest2()
    {
        List<String> text = new ArrayList<>();
        text.add("This");
        text.add("is");
        text.add("a");
        text.add("test.");
        String expected = "This";
        fileIO.writeFile(text);
        List<String> readText = fileIO.readFile();
        String actual = readText.get(0);
        Assert.assertEquals(expected, actual);
        expected = "a";
        actual = readText.get(2);
        Assert.assertEquals(expected, actual);
    }
}